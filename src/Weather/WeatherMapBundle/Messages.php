<?php

namespace Weather\WeatherMapBundle;


class Messages {

	const MSG_ERROR_404 = 'Error 404.';
	const CITY_NOT_PROVIDED = 'City is not entered. Please enter city to find the weather.';
	const MSG_SUCCESS = 'Weather information found successfully.';
}