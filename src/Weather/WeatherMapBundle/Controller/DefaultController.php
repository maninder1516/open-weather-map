<?php

namespace Weather\WeatherMapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Weather\WeatherMapBundle\Entity\CurrentWeather;
use Symfony\Component\HttpFoundation\JsonResponse;
use Weather\WeatherMapBundle\Messages;
use Psr\Log\LoggerInterface; // Include logging interface
use AppBundle\Utils\CommonFunctions as CFs;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class DefaultController extends Controller
{
    /**
     *  Get weather by city.
     *  @ApiDoc(
     *  resource=true,
     *  description="Get weather information by city name",
     *  parameters={
     *    {"name"="city", "dataType"="string", "required"=true, "description"="City name"},
     *  } 
     * )
     */
    public function indexAction(Request $request)
    {
    	$logger = $this->get('logger');
    	$logger->info('Inside index method.');
    	// Get the  city name from the  $_GET parameters
    	$city = $request->query->get('city');
    	if ($city == '' || $city == null) {
    		$response = array('success' => false, 'data' => null, 'detail' => array('message' => Messages::CITY_NOT_PROVIDED, 'error' => array(Messages::CITY_NOT_PROVIDED)));
    		// Log the Error
    		$logger->error('City is not provided.');
            return new JsonResponse($response);
    	}

    	try {
    		// Get weather from OpenWeather API based on city
	    	$openWeather = $this->get('dwr_open_weather');
	        $weather = $openWeather->setType('Weather')->getByCityName($city); //Bangalore

	        // Get the required weather information
	    	$weatherType = $weather->description();
	    	// Convert kalvin To Celsius
            $temp = CFs::convertKalvinToCelsius($weather->temp());
	    	$windSpeed = $weather->windSpeed();
	    	$windDeg = $weather->windDeg();

	    	// Get the wind direction from degree
            $windDirection = CFs::getWindDirectionFromWindDegree($windDeg);
	    	if($windDirection != null){
	    		$wind = (object) [
				    'speed' => $windSpeed,
			    	'direction' => $windDirection
				];
	    	}
			
	        // Create the Model Response
	        $currentWeather = new CurrentWeather();
	        $currentWeather->setWeatherType($weatherType);
	        $currentWeather->setTemperature($temp);
	        $currentWeather->setWind($wind);

	        // Create JSON response
	        $currentWeather = $currentWeather->jsonSerializedData();
	       
	        $response = array('success' => true, 'data' =>  $currentWeather, 'detail' => array('message' => Messages::MSG_SUCCESS, 'error' => null));
            return new JsonResponse($response);
    	}catch (\Exception $ex) {
    		$response = array('success' => false, 'data' => null, 'detail' => array('message' => Messages::MSG_ERROR_404, 'error' => array($ex)));
    		// Log the Error
    		$logger->critical('Error while getting the weather of city.', [
    			'cause' => 'ERROR :'.$ex 
    		]);
            return new JsonResponse($response);
    	}	
    }
}
