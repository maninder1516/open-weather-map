<?php

namespace Weather\WeatherMapBundle\Entity;

use JsonSerializable;

/**
 * CurrentWeather
 */
class CurrentWeather
{
    
    /**
     * @var int
     * @SWG\Property(description="The unique identifier of the Current Weather.")
     */
    private $id;

    /**
     * @SWG\Property(type="string", maxLength=255)
     */
    private $weatherType;

    /**
     * @var int
     */
    private $temperature;

    /**
     * @var \stdClass
     */
    private $wind;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weatherType
     *
     * @param string $weatherType
     *
     * @return CurrentWeather
     */
    public function setWeatherType($weatherType)
    {
        $this->weatherType = $weatherType;

        return $this;
    }

    /**
     * Get weatherType
     *
     * @return string
     */
    public function getWeatherType()
    {
        return $this->weatherType;
    }

    /**
     * Set temperature
     *
     * @param integer $temperature
     *
     * @return CurrentWeather
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return int
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set wind
     *
     * @param \stdClass $wind
     *
     * @return CurrentWeather
     */
    public function setWind($wind)
    {
        $this->wind = $wind;

        return $this;
    }

    /**
     * Get wind
     *
     * @return \stdClass
     */
    public function getWind()
    {
        return $this->wind;
    }

    /**
     *  Get JSON Serialized Data
     *
     */
    public function jsonSerializedData()
    {
        return array(
            'weather_type' => $this->getWeatherType(),
            'temparature'=> $this->getTemperature(),
            'wind'=> (object) $this->getWind(),
        );
    }
}

