<?php
// src/AppBundle/Utils/CommonFunctions.php 

namespace AppBundle\Utils;


class CommonFunctions {
	/*
     *  Find the wind direction from degree
     */
    public function getWindDirectionFromWindDegree($windDeg) {
    	if($windDeg != '' && $windDeg > 0){
    		$val = ($windDeg / 45) + 0.5;
	    	$arr = ["North","NorthEast","East", "SouthEast","South","SouthWest","West","NorthWest"];
		    return $arr[($val % 8)];
    	}else {
    		return null;
    	}	
    }

    /*
     *  Convert the weather temperature from kalvin to Celsius
     */
    public function convertKalvinToCelsius($temp) {
    	if($temp != '' && $temp > 0){
    		return round($temp - 273.15); // Convert kalvin To Celsius
    	}else {
    		return null;
    	}	
    }

    /*
     *  Slugify the string
     */
	public function slugify($string)
    {
        return preg_replace(
            '/[^a-z0-9]/', '-', strtolower(trim(strip_tags($string)))
        );
    }
}